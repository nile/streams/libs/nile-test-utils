package ch.cern.nile.test.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.AccessLevel;
import lombok.Getter;

import ch.cern.nile.common.configuration.properties.ClientProperties;
import ch.cern.nile.common.configuration.properties.DecodingProperties;
import ch.cern.nile.common.exceptions.MissingPropertyException;
import ch.cern.nile.common.json.JsonSerde;
import ch.cern.nile.common.models.Application;
import ch.cern.nile.common.streams.AbstractStream;

/**
 * Base class for testing stream decoders.
 * It provides a test driver and methods to pipe records to the input topic and read records from the output topic.
 * Extend this class and implement the {@link #createStreamInstance()} method to create an instance of the
 * stream decoder to test.
 */
@SuppressWarnings({"PMD.TooManyMethods", "PMD.ExcessiveImports", "PMD.CouplingBetweenObjects"})
public abstract class StreamTestBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(StreamTestBase.class);

    private static final Serializer<String> KEY_SERIALIZER = Serdes.String().serializer();
    private static final Serializer<JsonObject> VALUE_SERIALIZER = new JsonSerde().serializer();
    private static final Deserializer<String> KEY_DESERIALIZER = Serdes.String().deserializer();
    private static final Deserializer<JsonObject> VALUE_DESERIALIZER = new JsonSerde().deserializer();

    @Getter(AccessLevel.PROTECTED)
    private static final String SOURCE_TOPIC = getSourceTopicName();
    @Getter(AccessLevel.PROTECTED)
    private static final String DLQ_TOPIC = "lora-mqtt-test-dlq";
    private static final String DEFAULT_SINK_TOPIC = getSinkTopicName();
    private static final List<String> ROUTING_TOPICS = getRoutingTopicsNames();
    private static final String ROUTING_PROPERTIES_PATH = "/routing.properties";
    private static final String CONFIG_JSON_PATH = "/config.json";

    private final Map<String, TestOutputTopic<String, JsonObject>> outputTopics = new java.util.HashMap<>();
    private TopologyTestDriver testDriver;
    private TestInputTopic<String, JsonObject> inputTopic;


    private static Properties getProperties() {
        final Properties testDriverProperties = new Properties();
        testDriverProperties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "test-app");
        testDriverProperties.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9091");
        testDriverProperties.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,
                Serdes.String().getClass().getName());
        testDriverProperties.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, JsonSerde.class.getName());
        testDriverProperties.setProperty(StreamsConfig.STATE_DIR_CONFIG, "target/kafka-streams");
        return testDriverProperties;
    }

    private static String getTopicName(final String propertyKey) {
        final Properties routingProperties = new Properties();
        try (InputStream routingPropertiesStream = StreamTestBase.class.getResourceAsStream(ROUTING_PROPERTIES_PATH)) {
            routingProperties.load(routingPropertiesStream);
        } catch (IOException cause) {
            final String message = "Error while loading routing properties";
            LOGGER.error(message, cause);
            throw new MissingPropertyException(message, cause);
        }
        return routingProperties.getProperty(propertyKey);
    }

    private static String getSourceTopicName() {
        return getTopicName(ClientProperties.SOURCE_TOPIC.getValue());
    }

    private static String getSinkTopicName() {
        return getTopicName(DecodingProperties.SINK_TOPIC.getValue());
    }

    private static List<String> getRoutingTopicsNames() {
        List<String> topics = Collections.emptyList();
        try (InputStreamReader reader = new InputStreamReader(
                Objects.requireNonNull(StreamTestBase.class.getResourceAsStream(CONFIG_JSON_PATH)),
                StandardCharsets.UTF_8)) {

            // Use Gson to parse the JSON from the reader
            final Gson gson = new Gson();
            final Type listOfAppsType = new TypeToken<List<Application>>() {
            }.getType();
            final List<Application> apps = gson.fromJson(reader, listOfAppsType);
            topics = apps.stream().map(app -> app.getTopic().getName()).collect(Collectors.toList());
        } catch (IOException cause) {
            LOGGER.info("Error while loading routing properties", cause);
        }
        return topics;
    }

    /**
     * Creates an instance of the stream application to test.
     *
     * @return The stream decoder instance
     */
    public abstract AbstractStream createStreamInstance();

    @BeforeEach
    @SuppressWarnings("PMD.AvoidAccessibilityAlteration")
    void before() throws IOException, ReflectiveOperationException {
        final StreamsBuilder builder = new StreamsBuilder();

        final Properties routingProperties = new Properties();
        try (InputStream routingPropertiesStream = getClass().getResourceAsStream(ROUTING_PROPERTIES_PATH)) {
            routingProperties.load(routingPropertiesStream);
        }

        final AbstractStream stream = createStreamInstance();
        stream.configure(routingProperties);

        setTopics(stream);

        stream.configure(routingProperties);
        stream.createTopology(builder);

        final Topology topology = builder.build();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Created topology: {}\n", topology.describe());
        }

        final Properties testDriverProperties = getProperties();
        testDriver = new TopologyTestDriver(topology, testDriverProperties);

        inputTopic = testDriver.createInputTopic(SOURCE_TOPIC, KEY_SERIALIZER, VALUE_SERIALIZER);
        outputTopics.put(DEFAULT_SINK_TOPIC,
                testDriver.createOutputTopic(DEFAULT_SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER));
        outputTopics.put(DLQ_TOPIC, testDriver.createOutputTopic(DLQ_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER));
        for (final String topic : ROUTING_TOPICS) {
            outputTopics.put(topic, testDriver.createOutputTopic(topic, KEY_DESERIALIZER, VALUE_DESERIALIZER));
        }
    }

    private static void setTopics(final AbstractStream stream) throws ReflectiveOperationException {
        Class<?> clazz = stream.getClass().getSuperclass();
        if (!trySetField(clazz, stream, "sourceTopic", SOURCE_TOPIC)
                || !trySetField(clazz, stream, "sinkTopic", DEFAULT_SINK_TOPIC)) {
            clazz = clazz.getSuperclass();
            trySetField(clazz, stream, "sourceTopic", SOURCE_TOPIC);
            trySetField(clazz, stream, "sinkTopic", DEFAULT_SINK_TOPIC);
        }
    }

    @SuppressWarnings("PMD.AvoidAccessibilityAlteration")
    private static boolean trySetField(final Class<?> clazz, final Object target, final String fieldName,
                                       final Object fieldValue)
            throws ReflectiveOperationException {
        boolean result = false;
        try {
            final Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(target, fieldValue);
            result = true;
        } catch (NoSuchFieldException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Field {} not found in class {}", fieldName, clazz.getName());
            }
        }
        return result;
    }

    @AfterEach
    void after() throws IOException {
        testDriver.close();

        final Path directory = Paths.get("target/kafka-streams/");
        if (Files.exists(directory)) {
            try (Stream<Path> paths = Files.walk(directory)) {
                paths.sorted(Comparator.reverseOrder()).forEach(path -> {
                    try {
                        Files.delete(path);
                    } catch (IOException e) {
                        final String message = String.format("Error while deleting file %s", path);
                        LOGGER.error(message, e);
                        throw new IllegalStateException(message, e);
                    }
                });
            }
        }
    }

    /**
     * Pipes a record to the input topic of the test driver using TestInputTopic.
     *
     * @param record The record to pipe.
     */
    protected void pipeRecord(final JsonObject record) {
        inputTopic.pipeInput(record);
    }

    /**
     * Reads a record from the predefined output topic of the test driver.
     *
     * @return The next record from the output topic or null if the topic is empty
     */
    protected ProducerRecord<String, JsonObject> readRecord(final String topic) {
        final KeyValue<String, JsonObject> keyValue = outputTopics.get(topic).readKeyValue();

        return new ProducerRecord<>(topic, keyValue.key, keyValue.value);
    }

    /**
     * Reads a record from the output topic of the test driver.
     *
     * @return The record read
     */
    protected ProducerRecord<String, JsonObject> readRecord() {
        return readRecord(DEFAULT_SINK_TOPIC);
    }

}
