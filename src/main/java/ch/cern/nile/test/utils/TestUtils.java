package ch.cern.nile.test.utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.cern.nile.common.exceptions.MissingPropertyException;

/**
 * Utility class for testing.
 */
@SuppressWarnings("PMD.TestClassWithoutTestCases")
public final class TestUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestUtils.class);

    private TestUtils() {
    }

    /**
     * Create a JsonElement from the given data.
     *
     * @param data the data to be converted to a JsonElement
     * @return the JsonElement
     */
    public static JsonElement getDataAsJsonElement(final String data) {
        return JsonParser.parseString(String.format("\"%s\"", data));
    }

    /**
     * Loads a JSON resource from the classpath.
     *
     * @param path the path to the resource
     * @return the JSON object
     */
    public static JsonObject loadRecordAsJson(final String path) {
        final URL resourceUrl = ClassLoader.getSystemResource(path);
        try {
            final String content = Files.readString(Paths.get(resourceUrl.toURI()));
            return JsonParser.parseString(content).getAsJsonObject();
        } catch (IOException | URISyntaxException cause) {
            final String message = String.format("Failed to load %s", path);
            LOGGER.error(message, cause);
            throw new MissingPropertyException(message, cause);
        }
    }

}
