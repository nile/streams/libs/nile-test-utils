# Nile Test Utils

**Nile Test Utils** is a Java testing utility designed to facilitate the development and testing of Kafka streaming
applications. This utility provides a foundational framework for unit testing Kafka Streams topologies, particularly
focusing on JSON message decoding. It leverages the Kafka Streams test utilities and JUnit 5 framework, offering an
efficient way to set up, execute, and tear down stream topologies.

## Core Features

| Feature                           | Description                                                                                                  |
|-----------------------------------|--------------------------------------------------------------------------------------------------------------|
| **StreamTestBase Class**          | Abstract class that sets up the `TopologyTestDriver` for testing Kafka Streams topologies.                   |
| **Easy Topology Testing**         | Enables push-pull interaction with Kafka Streams topologies without needing a real Kafka broker.             |
| **JSON Message Processing**       | Integrated support for encoding and decoding JSON messages using the `JsonSerde` utility from `nile-common`. |
| **Customizable Testing**          | Abstract methods for subclasses to provide their specific streaming topologies.                              |
| **Efficient Resource Management** | Automatic cleanup of test drivers and resources after each test.                                             |

## Getting Started

### Prerequisites

- Java 21 or higher
- [Nile Common](https://gitlab.cern.ch/nile/streams/libs/nile-common) library dependency

### Adding Dependency

To include `nile-test-utils` in your project, add the following dependency to your `pom.xml` file:

```xml
<dependency>
    <groupId>ch.cern.nile</groupId>
    <artifactId>nile-test-utils</artifactId>
    <version>1.1.0</version>
</dependency>
```

Ensure that your Maven project is configured to access the GitLab Maven repository where `nile-test-utils` is hosted:

```xml
<repositories>
    <repository>
        <id>gitlab-maven-nile-test-utils</id>
        <url>https://gitlab.cern.ch/api/v4/projects/171116/packages/maven</url>
        <releases>
            <enabled>true</enabled>
        </releases>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
    </repository>
</repositories>
```

## Usage Example

### Testing Streaming Applications

To use `nile-test-utils` for your Kafka Streams testing:

1. **Extend the `StreamTestBase` Class:**
   Create a test class that extends `StreamTestBase` and implement the `createStreamInstance()` method to
   provide the Kafka Streams topology you want to test.

    ```java
    import ch.cern.nile.test.utils.StreamTestBase;
   
    class TestStreamTest extends StreamTestBase {
        @Override
        public TestStream createStreamDecodeInstance() {
            // Return an instance of your custom stream application
            return new TestStream();
        }
    }
    ```

2. **Write Test Methods:**
   Utilize provided methods like `pipeRecord(JsonObject record)`
   or `waitForEmptyOutputTopicAndPipeRecord(JsonObject record)` to simulate sending messages to the input topic
   and `readRecord()` or `readRecord(String topic)` to retrieve processed messages from the output topic.

   ```java
   import ch.cern.nile.test.utils.StreamTestBase;
   
   class TestStreamTest extends StreamTestBase {
   
       private static final JsonObject RECORD = TestUtils.loadRecordAsJson("data/record_correct.json");
       
       @Override
       public TestStream createStreamDecodeInstance() {
           // Return an instance of your custom stream application
           return new TestStream();
       }
   
       @Test
       void testStreamProcessing() {
           // Example of piping a record into the stream topology
           pipeRecord(RECORD);
   
           // Retrieve and assert on the processed record
           ProducerRecord<String, JsonObject> result = readRecord();
           assertNotNull(result);
           // Additional assertions as needed
       }
   }
   ```
## Documentation

Javadoc documentation for Nile Test Utils can be found [here](https://nile-test-utils.docs.cern.ch).

## Support & Contact

For support, questions, or feedback regarding Nile Test Utils, please
contact [Nile Support](mailto:nile-support@cern.ch).
